package com.example.proyecto_gsi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.proyecto_gsi.databinding.ActivityMainBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {
    //Binding
    ActivityMainBinding binding;
    //Sharedpreferences
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editorPreferences;

    //USER INFORMATION
    public int userIdValue;
    public static JSONObject userInformation;
    //Volley
    RequestQueue queue;

    public String USER_ID_KEY = "com.example.proyecto_gsi.USER_ID_KEY";
    final String TAG_ERROR = "BAD";
    final String TAG = "GOOD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        queue = Volley.newRequestQueue(this);

        checkUserID();

        //Creamos los intent de cada boton
        //UsernameActivity intent
        binding.usernameButton.setOnClickListener(view1 -> {
            //Creamos el intent
            Intent usernameIntent = new Intent(this, UsernameActivity.class);

            //Pasamos el nombre del usuario por el intent
            if(userInformation != null){

                try {
                    usernameIntent.putExtra("USERNAME", userInformation.getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //lo lanzamos
            startActivity(usernameIntent);
        });

        //MapsActivity intent
        binding.startButton.setOnClickListener(view1 -> {
            //Creamos el intent
            Intent mapActivity = new Intent(this, MapsActivity.class);
            //lo lanzamos
            startActivity(mapActivity);
        });
    }


    private void checkUserID(){
        //Comprobamos que el usuario no se ha logueado y en caso contrario le logueamos
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        userIdValue = sharedPref.getInt(USER_ID_KEY, -1);

        if(userIdValue != -1){
            getUserData();
            Log.i(TAG, "El usuario ya estaba registrado, su id es: "+ String.valueOf(userIdValue) + "y se estan recuperando sus datos");



        }else{
            Log.i(TAG_ERROR, "El usuario no se ha registrado");
            //Si el usuario no se ha creado nos logueamos
            autoLogin();
        }

    }

    private void autoLogin(){

        //URLS
        final String POST_NEW_USER_URL ="http://davidiriondo.eu.pythonanywhere.com/user";

        //Modelo de datos para crear automaticamente un usuario
        JSONObject userData = new JSONObject();

        try{
            userData.put("name", "Default");
            userData.put("surname", "Default");
            userData.put("phone_number", "000000000");

        }catch (JSONException j){
            Log.i(TAG_ERROR, j.getMessage());
        }

        // Hacemos la solicitud para agregar el usuario a la base de datos

        JsonObjectRequest logRequest = new JsonObjectRequest(Request.Method.POST, POST_NEW_USER_URL, userData,
                response ->{
                    try {

                        Log.i(TAG, response.getString("name"));
                        //Guardamos el id del usuario para identificarlo mas adelante
                        saveCredentials(Integer.parseInt(response.getString("id")));
                        userInformation = response;
                        Toast.makeText(this, "El usuario se ha registrado correctamente", Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    NetworkResponse response = error.networkResponse;
                    String errorMsg = "";
                    if(response != null && response.data != null){
                        String errorString = new String(response.data);
                        Log.i(TAG_ERROR, errorString);
                        Toast.makeText(this, "Ha ocurrido un error al registrar el usuario, reinicie la aplicacion", Toast.LENGTH_LONG).show();
                    }
                    else{
                        Log.i(TAG_ERROR, "NO HAY NINGUN ERROR");
                    }
                });


        // Add the request to the RequestQueue.
        queue.add(logRequest);
    }

    public void getUserData(){

        String GET_USER_DATA_URL ="http://davidiriondo.eu.pythonanywhere.com/user/" + String.valueOf(userIdValue);

        JsonObjectRequest r = new JsonObjectRequest(Request.Method.GET, GET_USER_DATA_URL, null,
                response ->{
                    userInformation = response;
                    try {
                        Log.i(TAG, "Se ha guardado la informacion del usuario [GET] " + userInformation.getString("name"));
                        Toast.makeText(this, "El usuario se ha logeado correctamente", Toast.LENGTH_LONG).show();
                        //EN ESTE PUNTO HAY QUE GUARDAR EL NOMBRE DEL USUARIO PARA RECUPERARLO MAS TARDE
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error ->{

                    Log.i(TAG_ERROR, "No se ha guardado la informacion del usuario, ha ocurrido un error [GET] ");
                    Toast.makeText(this, "Ha ocurrido un error en el login", Toast.LENGTH_LONG).show();
                });

        queue.add(r);
    }

    private void saveCredentials(int userID){
        SharedPreferences sharedPref = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(USER_ID_KEY, userID);
        editor.apply();
    }




}