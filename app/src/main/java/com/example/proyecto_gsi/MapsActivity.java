package com.example.proyecto_gsi;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.proyecto_gsi.databinding.ActivityMapsBinding;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class MapsActivity extends FragmentActivity implements  OnMapReadyCallback, EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {

        public static final int RC_COARSE_AND_FINE_LOCATION = 1;
        public static final int RC_CHECK_SETTINGS = 2;
        // Required permissions.
        public static final String[] PERMS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        // User name
        private String userName;
        // Activity View Binding
        private ActivityMapsBinding binding;


        private static final String TAG = "GOOD";
        private static final String ENDPOINT_URL = "http://davidiriondo.eu.pythonanywhere.com/user";
        private static final String ENDPOINT_GET_ALL_USERS = "http://davidiriondo.eu.pythonanywhere.com/all_users";
        private String installationId;
        private LocationRequest locationRequest;
        private FusedLocationProviderClient fusedLocationClient;
        private LocationCallback locationCallback;
        private GoogleMap map;
        private RequestQueue queue;



        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_maps);

            queue = Volley.newRequestQueue(this);
            checkPermissions();

            //Localizacion de usuario
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


            // Initiate the check permissions flow.
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

            // Forward results to EasyPermissions
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        }

        @AfterPermissionGranted(RC_COARSE_AND_FINE_LOCATION)
        public void checkPermissions() {

            if (EasyPermissions.hasPermissions(this, PERMS)) {
                // Already have permission, do the thing

                continueHavingLocationPermissions();

                // Create instances of the app fragments
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);


            } else {
                // Do not have permissions, request them now
                EasyPermissions.requestPermissions(this, getString(R.string.coarse_and_fine_location_rationale), RC_COARSE_AND_FINE_LOCATION, PERMS);
            }
        }


        @SuppressLint("MissingPermission")
        @Override
        public void onMapReady(@NonNull GoogleMap googleMap) {
        // This callback will be used by the requestLocationUpdates of the fusedLocationClient
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                Location location = locationResult.getLastLocation();

                // Update UI with location data
                LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());

                map.setMyLocationEnabled(true);

                //PODEMOS EMPEZAR A PEDIR LOCALIZACION de los demas
                double lat = myLocation.latitude ;
                double lon = myLocation.longitude;

                //Actualizamos nuestra posicion
                sendLocation(lat, lon);
                //Pedimos la posicion del resto de usuarios y las pintamos
                getUsersLocations();

                }
            };

            // Initiate the location updates procedure
            requestLocationUpdates();

            // Map setup, initial coordinates, zoom and controls.
            map = googleMap;



        }

        @Override
        public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) { }

        @Override
        public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
            // Check if the permission has been permanently denied, and redirect the user to the app settings.
            if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
                new AppSettingsDialog.Builder(this).build().show();


            } else {
                // Show a fragment explaining that we really need the permissions to work.

            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE || requestCode == RC_CHECK_SETTINGS) {
                // The app has returned from the app settings, call again checkPermissions.
                checkPermissions();
            }
        }

        @Override
        public void onRationaleAccepted(int requestCode) { }

        @Override
        public void onRationaleDenied(int requestCode) {

        }

        // At this point we have full location permissions.
        public void continueHavingLocationPermissions() {
            //Pedimos la localizacion del usuario
        }


        // Prepare the required instances and settings for receiving location updates.
        public void requestLocationUpdates() {
            // Create a FusedLocationClient
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

            // Prepare a LocationRequest that will be used to check if the device can satisfy the
            // requirements specified on the LocationRequest.
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            // Check the device settings for the locationRequest.
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            SettingsClient client = LocationServices.getSettingsClient(this);
            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

            // Device can comply with our requirements, so from now on we can request location updates from the fused location client.
            task.addOnSuccessListener(this, (locationResponse) -> {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                startLocationUpdates();
            });

            // This callback will be called for example when the user has disabled the GPS
            task.addOnFailureListener(this, (e) -> {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(this, RC_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            });
        }

        // Start the location updates via the fusedLocationClient.
        @SuppressLint("MissingPermission")
        private void startLocationUpdates() {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        }

        // Stop the location updates.
        private void stopLocationUpdates() {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        }

        private void sendLocation(double lat, double lon) {
            Log.d(TAG, String.format("HACEMOS LA ACTUALIZACION DE UBICACION"));

            JSONObject data = new JSONObject();

            try {
                data.put("id", MainActivity.userInformation.getString("id"));
                data.put("longitude", String.valueOf(lon));
                data.put("latitude", String.valueOf(lat));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.PATCH, ENDPOINT_URL , data,
                    response -> {
                        // Dibujar en el mapa los marks de mis amigos
                        Log.d(TAG, "LOCALIZACION ACTUALIZADA");
                        try {

                            LatLng userLocation = new LatLng(Double.parseDouble(response.getString("lat")),Double.parseDouble(response.getString("long")));
                            map.addMarker(new MarkerOptions().position(userLocation).title(response.getString("name")));
                            map.moveCamera(CameraUpdateFactory.newLatLng(userLocation));
                            map.moveCamera(CameraUpdateFactory.zoomTo(15));
                            map.getUiSettings().setZoomControlsEnabled(true);
                            map.getUiSettings().setCompassEnabled(true);

                        }catch (JSONException f){

                        }


                    },
                    error -> {
                        Log.e(TAG, "ERROR AL ACTUALIZAR LOCALIZACION");
                    });

            queue.add(request);
        }

        private void getUsersLocations(){
            JsonArrayRequest rq = new JsonArrayRequest(Request.Method.GET, ENDPOINT_GET_ALL_USERS, null,
                    response -> {
                        JSONObject user = new JSONObject();
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                user = response.getJSONObject(i);
                                LatLng userLocation = new LatLng(Double.parseDouble(user.getString("lat")),Double.parseDouble(user.getString("long")));
                                map.addMarker(new MarkerOptions().position(userLocation).title(user.getString("name")));
                                map.moveCamera(CameraUpdateFactory.newLatLng(userLocation));
                                map.animateCamera(CameraUpdateFactory.newLatLng(userLocation));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    error -> {
                        Log.i("USUARIOS", error.getMessage());
                    });

            queue.add(rq);

        }

        private void updateMap(JSONObject obj) {
            // Actualizamos el mapa.

        }

}