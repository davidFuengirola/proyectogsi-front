package com.example.proyecto_gsi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.proyecto_gsi.databinding.ActivityMainBinding;
import com.example.proyecto_gsi.databinding.ActivityUsernameBinding;

import org.json.JSONException;
import org.json.JSONObject;

public class UsernameActivity extends AppCompatActivity {
    ActivityUsernameBinding binding;
    RequestQueue queue;
    JSONObject user;

    final String URL = "http://davidiriondo.eu.pythonanywhere.com/user";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUsernameBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //User
        user = MainActivity.userInformation;

        //Instanciamos la cola
        queue = Volley.newRequestQueue(this);

        Intent intent = getIntent();
        String username = intent.getStringExtra("USERNAME");
        //Le asignamos el nombre
        TextView tv = binding.usernameField;
        tv.setText(username);

        //Cambiamos el nombre del usuario
        binding.changeUsernameButton.setOnClickListener(listener -> {

            JSONObject data = new JSONObject();
            try {
                data.put("id", user.getString("id"));
                data.put("name", binding.usernameField.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest r = new JsonObjectRequest(Request.Method.PATCH, URL,data,
                    response -> {
                        try {
                            tv.setText(data.getString("name"));
                            MainActivity.userInformation.put("name", data.getString("name"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(this, "SE HA ACTUALIZADO SU NOMBRE DE USUARIO", Toast.LENGTH_LONG).show();
                    },
                    error -> {
                        Toast.makeText(this, "ERROR ACTUALIZANDO EL NOMBRE", Toast.LENGTH_LONG).show();
                    });

            queue.add(r);

        });



    }
}